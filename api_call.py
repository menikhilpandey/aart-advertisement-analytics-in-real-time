import requests
import ast
import datetime

creds = open('credentials.txt')
app_id = creds.readline().strip()
app_key = creds.readline().strip()

base_url = 'https://api.kairos.com/v2/'
headers = {
    'Content-Type': 'application/json',
    'app_id' : app_id,
    'app_key': app_key
    }
payload = 'source=http://139.59.88.37/images/image.png'

def call():
    r = requests.post(base_url+'media?'+payload, headers=headers)
    if r.status_code == 200:
        out = ast.literal_eval(r.text)
        id_ = out['id']
        print 'Started '+id_
        analytics = requests.get(base_url+'analytics/'+id_, headers=headers)
        if analytics.status_code == 200:
            json_out = {
                "age_group": {
                    "Kid":0,
                    "Teenager":0,
                    "Young-Adult":0,
                    "Middle-Age-Adult":0,
                    "Senior":0
                },
                "gender": {
                    "Male":0,
                    "Female":0
                },
                "average_emotion": {
                    "anger": 0,
                    "sadness": 0,
                    "fear": 0,
                    "disgust": 0,
                    "joy": 0,
                    "surprise": 0
                  },
                  "emotion_score": {
                    "positive": 0,
                    "negative": 0,
                    "neutral": 0
                  }
            }
            analytics_out = ast.literal_eval(analytics.text)
            views = 0
            for item in analytics_out['impressions']:
                for engage in item["tracking"]:
                    views+=item["tracking"][engage]
                if 'average_emotion' in item:
                    for sub_item in item['average_emotion']:
                        if sub_item in json_out["average_emotion"]:
                            json_out["average_emotion"][sub_item]+=\
                            item['average_emotion'][sub_item]
                if 'emotion_score' in item:
                    for sub_item in item['emotion_score']:
                        if sub_item in json_out["emotion_score"]:
                            json_out["emotion_score"][sub_item]+=\
                            item['emotion_score'][sub_item]
                if 'demographics' in item:
                    item_age_group = item['demographics']['age_group']
                    if item_age_group in json_out['age_group']:
                        json_out['age_group'][item_age_group]+=1
                    gender = item['demographics']['gender']
                    if item_gender in json_out['gender']:
                        json_out['gender'][item_gender]+=1
            json_out["views"] = views
            dt_stamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            json_out["timestamp"] = dt_stamp
            return json_out
        else:
            print 'Error, Request for gettting analytics unsuccessful.'
        delete = requests.delete(base_url+'media/'+id_, headers=headers)
        if delete.status_code == 200:
            del_out =  ast.literal_eval(delete.text)
            print 'Completed '+del_out['id']
        else:
            print 'Error, Could not complete deletion request.'

    else:
        print 'Error, Post Request unsuccessful.'
