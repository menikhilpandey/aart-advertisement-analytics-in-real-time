1. Engagement Rate - Viewed/Not Viewed Audience
-> Total faces / Total humans
2. Senitment trend - Positive, Neutral and Negative Sentiment trend
-> From face emotion detection
3. Brand Equity - views of that brand/total views of all brands
-> mapping views with pre existing brand information
4. Impressions trend - No of views/minute/hour/day
-> Total faces == views, rolling up according to time
5. Source Location Heatmap - more heat at location with more views
-> mapping with pre existing location information
6. Gender analytics - Men views, women views
-> using face recognition api
7. Age analytics - age of person viewing
-> Age Group Analytics
8. New and Returning users - cosine similarity
-> Matching current users faces with past users faces
9. View Time - time for which the face viewed the ad
-> can be calculated as: (Vi+1*(if face)+Vi(face)-Vi-1) for i++
10. Emotions Trend
-> using api
