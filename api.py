import api_call
import json
from flask import Flask
app = Flask(__name__)

@app.route('/')
def output():
    out_main = api_call.call()
    if out_main:
        return json.dumps(out_main)
    return 'Success'
