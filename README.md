# AART - Advertisement Analytics in Real Time #

### Overview ###

AART is a first ever platform for monitoring the performance of offline advertisements in real time. 

### Tech-Stack ###

* Deep Learning (CNNs)
* Computer Vision (CV)
* Web App (MEAN Stack)
* API (Python Flask)  

### KPIs Calculated ###

* Engagement Rate (No of Persons Viewed/ Total Number of Persons Detected)
* Senitment trend (Positive, Neutral and Negative Sentiment trend)
* Brand Equity (Total views of that brand/total views of all brands
* Impressions trend (No of views/minute/hour/day)
* Source Location Heatmap (More heat at locations with more views)
* Gender analytics (Gender Wise Views to understand interested audience)
* Age analytics (Average Age of persons who viewed ad at a particular location)
* New and Returning users (Using Face Recognition with Cosine Similarity on facial features)
* View Time (Avg view time for ads)
* Emotions Trend (Trend of emotions with time) 